# frozen_string_literal: true

# Auto-require all cops under `gitlab/styles/rubocop/cop/**/*.rb`
cops_glob = File.join(__dir__, 'rubocop', 'cop', '**', '*.rb')
Dir[cops_glob].sort.each { |cop| require(cop) }

module Gitlab
  module Styles
    module Rubocop
    end
  end
end
