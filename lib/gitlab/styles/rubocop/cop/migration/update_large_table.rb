# frozen_string_literal: true
require_relative '../../migration_helpers'

module Gitlab
  module Styles
    module Rubocop
      module Cop
        module Migration
          # This cop checks for methods that may lead to batch type issues on a table that's been
          # explicitly denied because of its size.
          #
          # Even though though these methods perform functions to avoid
          # downtime, using it with tables with millions of rows still causes a
          # significant delay in the deploy process and is best avoided.
          #
          # See https://gitlab.com/gitlab-com/infrastructure/issues/1602 for more
          # information.
          class UpdateLargeTable < RuboCop::Cop::Cop
            include MigrationHelpers

            MSG = 'Using `%s` on the `%s` table will take a long time to ' \
              'complete, and should be avoided unless absolutely ' \
              'necessary'

            def_node_matcher :batch_update?, <<~PATTERN
              (send nil? ${#denied_method?}
                (sym $...)
                ...)
            PATTERN

            def on_send(node)
              return if denied_tables.empty? || denied_methods.empty?
              return unless in_migration?(node)

              matches = batch_update?(node)
              return unless matches

              update_method = matches.first
              table = matches.last.to_a.first

              return unless denied_tables.include?(table)

              add_offense(node, message: format(MSG, update_method, table))
            end

            private

            def denied_tables
              cop_config['DeniedTables'] || []
            end

            def denied_method?(method_name)
              denied_methods.include?(method_name)
            end

            def denied_methods
              cop_config['DeniedMethods'] || []
            end
          end
        end
      end
    end
  end
end
