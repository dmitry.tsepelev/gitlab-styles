# frozen_string_literal: true

require_relative '../model_helpers'

module Gitlab
  module Styles
    module Rubocop
      module Cop
        # Cop that prevents the use of polymorphic associations
        class PolymorphicAssociations < RuboCop::Cop::Cop
          include ModelHelpers

          MSG = 'Do not use polymorphic associations, use separate tables instead'

          def on_send(node)
            return unless in_model?(node)
            return unless node.children[1] == :belongs_to

            node.children.last.each_node(:pair) do |pair|
              key_name = pair.children[0].children[0]

              add_offense(pair) if key_name == :polymorphic
            end
          end
        end
      end
    end
  end
end
