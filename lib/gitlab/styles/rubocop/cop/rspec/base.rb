# frozen_string_literal: true

require 'rubocop-rspec'
require_relative '../../rspec/helpers'

module Gitlab
  module Styles
    module Rubocop
      module Cop
        module RSpec
          class Base < RuboCop::Cop::RSpec::Base
            include Rubocop::Rspec::Helpers
          end
        end
      end
    end
  end
end
