# frozen_string_literal: true

require 'rubocop-rspec'

module Gitlab
  module Styles
    module Rubocop
      module Rspec
        module Helpers
          include RuboCop::RSpec::Language

          LET_IT_BE_HELPERS = SelectorSet.new(%i[let_it_be let_it_be_with_refind let_it_be_with_reload])
          LET = LET_IT_BE_HELPERS + RuboCop::RSpec::Language::Helpers::ALL
        end
      end
    end
  end
end
