# frozen_string_literal: true

module Gitlab
  module Styles
    VERSION = '6.4.0'
  end
end
