# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../../lib/gitlab/styles/rubocop/cop/active_record_serialize'

RSpec.describe Gitlab::Styles::Rubocop::Cop::ActiveRecordSerialize do
  subject(:cop) { described_class.new }

  context 'when inside the app/models directory' do
    it 'registers an offense when serialize is used', :aggregate_failures do
      allow(cop).to receive(:in_model?).and_return(true)

      expect_offense(<<~RUBY)
        serialize :foo
        ^^^^^^^^^ Do not store serialized data in the database, use separate columns and/or tables instead
      RUBY
    end
  end

  context 'when outside the app/models directory' do
    it 'does nothing' do
      allow(cop).to receive(:in_model?).and_return(false)

      expect_no_offenses(<<~RUBY)
        serialize :foo
      RUBY
    end
  end
end
