# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../../../lib/gitlab/styles/rubocop/cop/code_reuse/active_record'

RSpec.describe Gitlab::Styles::Rubocop::Cop::CodeReuse::ActiveRecord do
  subject(:cop) { described_class.new }

  it 'flags the use of "where" without any arguments' do
    expect_offense(<<~SOURCE)
    def foo
      User.where
           ^^^^^ This method can only be used inside an ActiveRecord model: https://gitlab.com/gitlab-org/gitlab-foss/issues/49653
    end
    SOURCE
  end

  it 'flags the use of "where" with arguments' do
    expect_offense(<<~SOURCE)
    def foo
      User.where(id: 10)
           ^^^^^ This method can only be used inside an ActiveRecord model: https://gitlab.com/gitlab-org/gitlab-foss/issues/49653
    end
    SOURCE
  end

  it 'does not flag the use of "group" without any arguments' do
    expect_no_offenses(<<~SOURCE)
    def foo
      project.group
    end
    SOURCE
  end

  it 'flags the use of "group" with arguments' do
    expect_offense(<<~SOURCE)
    def foo
      project.group(:name)
              ^^^^^ This method can only be used inside an ActiveRecord model: https://gitlab.com/gitlab-org/gitlab-foss/issues/49653
    end
    SOURCE
  end

  it 'autocorrects offenses in instance methods by allowing them' do
    expect_offense(<<~RUBY)
      def foo
        User.where
             ^^^^^ This method can only be used inside an ActiveRecord model: https://gitlab.com/gitlab-org/gitlab-foss/issues/49653
      end
    RUBY

    expect_correction(<<~RUBY)
      # rubocop: disable CodeReuse/ActiveRecord
      def foo
        User.where
      end
      # rubocop: enable CodeReuse/ActiveRecord
    RUBY
  end

  it 'autocorrects offenses in class methods by allowing them' do
    expect_offense(<<~RUBY)
      def self.foo
        User.where
             ^^^^^ This method can only be used inside an ActiveRecord model: https://gitlab.com/gitlab-org/gitlab-foss/issues/49653
      end
    RUBY

    expect_correction(<<~RUBY)
      # rubocop: disable CodeReuse/ActiveRecord
      def self.foo
        User.where
      end
      # rubocop: enable CodeReuse/ActiveRecord
    RUBY
  end

  it 'autocorrects offenses in blocks by allowing them' do
    expect_offense(<<~RUBY)
      get '/' do
        User.where
             ^^^^^ This method can only be used inside an ActiveRecord model: https://gitlab.com/gitlab-org/gitlab-foss/issues/49653
      end
    RUBY

    expect_correction(<<~RUBY)
      # rubocop: disable CodeReuse/ActiveRecord
      get '/' do
        User.where
      end
      # rubocop: enable CodeReuse/ActiveRecord
    RUBY
  end
end
