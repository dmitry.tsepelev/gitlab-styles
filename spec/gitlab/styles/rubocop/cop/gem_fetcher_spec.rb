# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../../lib/gitlab/styles/rubocop/cop/gem_fetcher'

RSpec.describe Gitlab::Styles::Rubocop::Cop::GemFetcher do
  subject(:cop) { described_class.new }

  def offense_msg(source)
    "#{'^' * source.length} Do not use gems from git repositories, only use gems from RubyGems."
  end

  context 'when in Gemfile' do
    before do
      allow(cop).to receive(:gemfile?).and_return(true)
    end

    it 'registers an offense when a gem uses `git`', :aggregate_failures do
      source = 'git: "https://gitlab.com/foo/bar.git"'

      expect_offense(<<~RUBY)
        gem "foo", git: "https://gitlab.com/foo/bar.git"
                   #{offense_msg(source)}
      RUBY
    end

    it 'registers an offense when a gem uses `github`', :aggregate_failures do
      source = 'github: "foo/bar.git"'

      expect_offense(<<~RUBY)
        gem "foo", github: "foo/bar.git"
                   #{offense_msg(source)}
      RUBY
    end
  end

  context 'when outside of Gemfile' do
    it 'registers no offense' do
      expect_no_offenses('gem "foo", git: "https://gitlab.com/foo/bar.git"')
    end
  end
end
