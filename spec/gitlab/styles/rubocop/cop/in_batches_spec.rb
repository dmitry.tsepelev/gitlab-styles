# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../../lib/gitlab/styles/rubocop/cop/in_batches'

RSpec.describe Gitlab::Styles::Rubocop::Cop::InBatches do
  subject(:cop) { described_class.new }

  it 'registers an offense when in_batches is used' do
    expect_offense(<<~RUBY)
      foo.in_batches do; end
          ^^^^^^^^^^ Do not use `in_batches`, use `each_batch` from the EachBatch module instead
    RUBY
  end
end
