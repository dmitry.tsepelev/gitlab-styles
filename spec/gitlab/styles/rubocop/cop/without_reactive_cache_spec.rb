# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../../lib/gitlab/styles/rubocop/cop/without_reactive_cache'

RSpec.describe Gitlab::Styles::Rubocop::Cop::WithoutReactiveCache do
  subject(:cop) { described_class.new }

  it 'registers an offense when without_reactive_cache is used' do
    expect_offense(<<~RUBY)
      without_reactive_cache do; end
      ^^^^^^^^^^^^^^^^^^^^^^ without_reactive_cache is for debugging purposes only. Please use with_reactive_cache.
    RUBY
  end
end
