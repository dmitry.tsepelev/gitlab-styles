# frozen_string_literal: true

require 'bundler/setup'
require 'gitlab/styles'

require 'rubocop'
require 'rubocop/rspec/support'
require 'rspec-parameterized'

spec_helper_glob = File.expand_path('support/*.rb', __dir__)
Dir.glob(spec_helper_glob).sort.each { |helper| require(helper) }

RSpec.configure do |config|
  config.order = :random

  config.expect_with :rspec do |expectations|
    expectations.syntax = :expect # Disable `should`
  end

  config.mock_with :rspec do |mocks|
    mocks.syntax = :expect # Disable `should_receive` and `stub`
  end

  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Forbid RSpec from monkey patching any of our objects
  config.disable_monkey_patching!

  # We should address configuration warnings when we upgrade
  config.raise_errors_for_deprecations!

  # RSpec gives helpful warnings when you are doing something wrong.
  # We should take their advice!
  config.raise_on_warning = true

  config.include(ExpectOffense)
end
